﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Space;

namespace SpaceClient
{
    class Program
    {
        static void Main(string[] args)
        {
            Starship ship = new Starship();
            ship.Captian = new Person("Jack Sparrow", 30);
            ship.Crew.Add(new Person("Joshamee Gibbs", 50));
            ship.Crew.Add(new Person("Jack, the Monkey", 3));
            ship.Crew.Add(new Person("Will Turner", 20));
            ship.Crew.Add(new Person("Elizabeth Swann", 20));
            ship.Crew.Add(new Person("Artur Dent", 30));

            PresentCrew(ship);
            Console.WriteLine();

            BlackHole vortex = new BlackHole();

            Console.WriteLine("The Ultimate Answer is {0}", vortex.UltimateAnswer());
            Console.WriteLine();

            var shipAfterPull = vortex.PullStarship(ship);
            Console.WriteLine("Presentation after pull:");
            PresentCrew(shipAfterPull);

            Console.ReadLine();
        }

        static void PresentCrew(Starship ship)
        {
            Console.WriteLine("Captain:");
            Console.WriteLine("{0}, {1} years old", ship.Captian.Name, ship.Captian.Age);
            Console.WriteLine("Crew:");
            foreach (var member in ship.Crew)
            {
                Console.WriteLine("{0}, {1} years old", member.Name, member.Age);
            }
        }
    }
}
