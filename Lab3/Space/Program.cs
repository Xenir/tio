﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Text;
using System.Threading.Tasks;

namespace Space
{
    public class Program
    {
        static void Main(string[] args)
        {
            Uri address = new Uri("http://localhost:9009/Space");

            ServiceHost selfHost = new ServiceHost(typeof(BlackHole), address);

            try
            {
                selfHost.AddServiceEndpoint(typeof (IBlackHole), new WSHttpBinding(), "BlackHoleEndpoint");
                ServiceMetadataBehavior behavior = new ServiceMetadataBehavior();
                behavior.HttpGetEnabled = true;
                selfHost.Description.Behaviors.Add(behavior);

                selfHost.Open();
                Console.WriteLine("Black Hole is open now!");
                Console.ReadLine();

                selfHost.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                selfHost.Abort();
            }
        }
    }
}
