﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Space
{
    public class BlackHole : IBlackHole
    {
        public Starship PullStarship(Starship ship)
        {
            if (ship.Captian.Age < 40)
            {
                foreach (var member in ship.Crew)
                    member.Age += 20;
                ship.Captian.Age += 20;
            }
            return ship;
        }

        public string UltimateAnswer()
        {
            return 42.ToString();
        }
    }
}
