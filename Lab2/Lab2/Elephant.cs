﻿namespace Lab2
{
    class Elephant : Animal
    {
        public Elephant(string name, float weight) :
            base(name, weight, false)
        {
        }

        public override string Sound()
        {
            return "Turuuuu";
        }

        public override string Trick()
        {
            return "Water pump";
        }

        public override int CountLegs()
        {
            return 4;
        }
    }
}
