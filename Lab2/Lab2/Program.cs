﻿using System;
using System.Linq;

namespace Lab2
{
    class Program
    {
        static void Main(string[] args)
        {
            Circus circus = new Circus("Monty Python's Flying Circus");
            Zoo zoo = new Zoo("Famous Zoo");

            var exit = false;
            while (!exit)
            {
                Console.WriteLine("Wybierz jedną z poniższych opcji:");
                Console.WriteLine("a) Prezentacja zwierząt w cyrku {0}", circus.Name);
                Console.WriteLine("b) Obejrzenie programu cyrku {0}", circus.Name);
                Console.WriteLine("c) Posłuchanie dźwięków zoo {0}", zoo.Name);
                Console.WriteLine("d) Wyświetla imię pierwszego znalezionego futrzaka w zoo {0}", zoo.Name);
                Console.WriteLine("e) Wyświetla wszystkie imiona zwierząt w cyrku {0}", circus.Name);
                Console.WriteLine("q) Kończy program");

                Console.WriteLine();
                var key = Console.ReadKey().KeyChar;
                Console.WriteLine();
                Console.WriteLine();

                switch (key)
                {
                    case 'a':
                        Console.WriteLine(circus.AnimalsIntroduction());
                        break;
                    case 'b':
                        Console.WriteLine(circus.Show());
                        break;
                    case 'c':
                        Console.WriteLine(zoo.Sounds());
                        break;
                    case 'd':
                        var ani = zoo.Animals.FirstOrDefault(x => x.HaveFur);
                        var name = ani != null ? ani.Name : "Żadne zwierzę nie posiada futra";
                        Console.WriteLine(name);
                        Console.WriteLine();
                        break;
                    case 'e':
                        foreach (var animal in circus.Animals)
                        {
                            Console.WriteLine(animal.Name);
                        }
                        Console.WriteLine();
                        break;
                    case 'q':
                        Console.WriteLine("Zakończenie wykonywania\n");
                        exit = true;
                        break;
                    default:
                        Console.Write("Niewłaściwy klawisz, podaj inną opcję\n");
                        break;
                }
            }
        }
    }
}
