﻿using System.Collections.Generic;

namespace Lab2
{
    class Zoo : IZoo
    {
        public List<Animal> Animals { get; set; }

        public string Name { get; set; }

        public Zoo(string name)
        {
            Name = name;
            Animals = new List<Animal>
            {
                new Giraffe("ZG1", 800f),
                new Elephant("ZS1", 4500f),
                new Elephant("ZS2", 3000f),
                new Pony("ZP1", 800f, false),
                new Giraffe("ZG2", 1200f),
                new Pony("ZP2", 400f, true),
                new Cat("ZC1", 4f, "black"),
                new Ant("ZA1", 0.00003f, true),
                new Ant("ZA2", 0.00001f, false),
                new Cat("ZC2", 400f, "ginger")
            };
        }

        public string Sounds()
        {
            var result = "";
            foreach (var animal in Animals)
            {
                result += animal.Sound() + "\n";
            }
            return result;
        }
    }
}
