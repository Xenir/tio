﻿namespace Lab2
{
    class Cat : Animal
    {
        public string Color { set; get; }

        public Cat(string name, float weight, string color) :
            base(name, weight, true)
        {
            Color = color;
        }

        public override string Sound()
        {
            return "Meow";
        }

        public override string Trick()
        {
            return "Begging for milk";
        }

        public override int CountLegs()
        {
            return 4;
        }
    }
}
