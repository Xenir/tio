﻿namespace Lab2
{
    class Ant : Animal
    {
        public bool IsQueen { set; get; }

        public Ant(string name, float weight, bool queen) :
            base(name, weight, false)
        {
            IsQueen = queen;
        }

        public override string Sound()
        {
            return "...";
        }

        public override string Trick()
        {
            return "Become 10 times larger in 1 second";
        }

        public override int CountLegs()
        {
            return 6;
        }
    }
}
