﻿namespace Lab2
{
    class Pony : Animal
    {
        public bool IsMagic { set; get; }

        public Pony(string name, float weight, bool magic) :
            base(name, weight, true)
        {
            IsMagic = magic;
        }

        public override string Sound()
        {
            return "Eeh-aah";
        }

        public override string Trick()
        {
            return "Riding through Brokilon unharmed";
        }

        public override int CountLegs()
        {
            return 4;
        }
    }
}
