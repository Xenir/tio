﻿namespace Lab2
{
    class Giraffe : Animal
    {
        public Giraffe(string name, float weight) :
            base(name, weight, true)
        {
        }

        public override string Sound()
        {
            return "???";
        }

        public override string Trick()
        {
            return "Mobile elevator";
        }

        public override int CountLegs()
        {
            return 4;
        }
    }
}
