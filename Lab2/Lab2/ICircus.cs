﻿namespace Lab2
{
    interface ICircus
    {
        string AnimalsIntroduction();

        string Show();

        int Patter(int howMuch);
    }
}
