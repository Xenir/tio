﻿using System.Collections.Generic;

namespace Lab2
{
    class Circus : ICircus
    {
        public List<Animal> Animals { get; set; }

        public string Name { get; set; }

        public Circus(string name)
        {
            Name = name;
            Animals = new List<Animal> {
                new Ant("CA", 0.000005f, false),
                new Cat("CC", 4f, "brown"),
                new Elephant("CS", 5000f),
                new Giraffe("CG", 1000f),
                new Pony("CP", 300f, true),
            };
        }

        public string AnimalsIntroduction()
        {
            var result = "";
            foreach (var animal in Animals)
            {
                result += animal.Sound() + "\n";
            }
            return result;
        }

        public string Show()
        {
            var result = "";
            foreach (var animal in Animals)
            {
                result += animal.Trick() + "\n";
            }
            return result;
        }

        public int Patter(int howMuch)
        {
            var sum = 0;
            foreach (var animal in Animals)
            {
                sum += animal.CountLegs();
            }
            return sum*howMuch;
        }
    }
}
