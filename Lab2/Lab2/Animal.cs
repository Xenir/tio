﻿namespace Lab2
{
    abstract class Animal
    {
        public string Name { get; set; }

        public float Weight { get; set; }

        public bool HaveFur { get; set; }

        public Animal(string name, float weight, bool fur)
        {
            Name = name;
            Weight = weight;
            HaveFur = fur;
        }

        public abstract string Sound();

        public abstract string Trick();

        public abstract int CountLegs();
    }
}
