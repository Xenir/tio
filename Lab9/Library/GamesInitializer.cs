﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library
{
    public class GamesInitializer : DropCreateDatabaseIfModelChanges<GamesContext>
    {
        protected override void Seed(GamesContext context)
        {
            var paintings = new List<Game>
            {
                new Game() {Title = "Wiedźmin", CreatorCompany = "CD Project RED", Year = 2007, AgeRate = 18},
                new Game() {Title = "Magicka", CreatorCompany = "Arrowhead Game Studios", Year = 2011, AgeRate = 16},
                new Game() {Title = "Diablo III", CreatorCompany = "Blizzard Entertainment", Year = 2012, AgeRate = 16},
                new Game() {Title = "Darkest Dungeon", CreatorCompany = "Red Hook Studios", Year = 2016, AgeRate = 12}
            };
            paintings.ForEach(x => context.Games.Add(x));
            context.SaveChanges();

            var artists = new List<Store>
            {
                new Store() {Name = "Saturn", Address = "ul. Ktorakolwiek 666, XX-YYY Gdziekolwiek"},
                new Store() {Name = "GOG", Address = "gog.com"},
                new Store() {Name = "Steam", Address = "steam.com"}
            };
            artists.ForEach(x => context.Stores.Add(x));
            context.SaveChanges();
        }
    }
}
