﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Lab7.Models;

namespace Lab7.DAL
{
    public class StoreContext : DbContext
    {
        public StoreContext()
            : base("StoreContext")
        {
            this.Configuration.LazyLoadingEnabled = false;
            this.Configuration.ProxyCreationEnabled = false;
        }

        public DbSet<Book> Books { get; set; }

        public DbSet<Author> Authors { get; set; }

    }
}
