﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab7.DAL;
using Lab7.Models;

namespace Lab7.DAL
{
    public class StoreInitializer : DropCreateDatabaseIfModelChanges<StoreContext>
    {
        protected override void Seed(StoreContext context)
        {
            var books = new List<Book>
            {
                new Book() {BookTitle = "Kroniki Czarnej Kompanii", ISBN = "978-83-7510-467-7", PageCount = 700},
                new Book() {BookTitle = "Kolor Magii", ISBN = "978-83-7648-564-5", PageCount = 200},
                new Book() {BookTitle = "Autostopem przez galaktykę", ISBN = "978-86-3650-570-0", PageCount = 300}
            };
            books.ForEach(x => context.Books.Add(x));
            context.SaveChanges();

            var authors = new List<Author>
            {
                new Author() {AuthorName = "Glen", AuthorSurname = "Cook"},
                new Author() {AuthorName = "Terry", AuthorSurname = "Pratchett"},
                new Author() {AuthorName = "Douglas", AuthorSurname = "Adams"}
            };
            authors.ForEach(x => context.Authors.Add(x));
            context.SaveChanges();
        }
    }
}
