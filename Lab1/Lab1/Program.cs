﻿using System;

namespace Lab1
{
    class Program
    {
        static void Main(string[] args)
        {
            ReversedInput ri = new ReversedInput();
            Console.WriteLine(ri.ProcessInput(args));
            Console.ReadKey();
        }
    }
}
