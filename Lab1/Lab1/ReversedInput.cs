﻿using System;

namespace Lab1
{
    class ReversedInput
    {
        public string ProcessInput(string[] input)
        {
            var result = "";
            Array.Reverse(input);
            for (var i = 0; i < input.Length; i++)
            {
                var chars = input[i].ToCharArray();
                Array.Reverse(chars);
                result += new string(chars) + (i != input.Length - 1 ? " " : "");
            }
            return result;
        }
    }
}
