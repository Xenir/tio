﻿using System;
using System.Collections.Generic;

namespace Lab1
{
    class LCM
    {
        public int ProcessInput(string[] input)
        {
            List<int> numbers = new List<int>();
            foreach (var word in input)
            {
                numbers.Add(Int32.Parse(word));
            }
            while (numbers.Count > 2)
            {
                var first = numbers[0];
                var second = numbers[1];
                numbers.RemoveAt(0);
                numbers.RemoveAt(0);
                numbers.Add(lcm(first, second));
            }
            return lcm(numbers[0], numbers[1]);
        }

        public int lcm(int a, int b)
        {
            return Math.Abs(a*b)/gcd(a, b);
        }

        public int gcd(int a, int b)
        {
            if (a == 0)
                return gcd(b, a);
            if (b == 0)
                return a;
            return gcd(b, a%b);
        }
    }
}
