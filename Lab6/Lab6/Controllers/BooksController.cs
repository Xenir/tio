﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using Database;
using Interfaces;
using Model;

namespace Lab6.Controllers
{
    public class BooksController : ApiController
    {
        private readonly IBooksRepository _repository;

        public BooksController()
        {
            _repository = new BooksRepository();
        }

        // GET api/books
        public IEnumerable<Book> Get()
        {
            return _repository.GetAll();
        }

        // GET api/books/5
        public Book Get(int id)
        {
            return _repository.Get(id);
        }

        public IEnumerable<Book> Get([FromUri] string search)
        {
            return _repository.GetAll().FindAll(x => x.BookTitle.Contains(search));
        }

        // POST api/books
        public int Post([FromUri]Book value)
        {
            _repository.Add(value);
            return value.Id;
        }

        // PUT api/books/5
        public void Put(int id, [FromUri]Book value)
        {
            value.Id = id;
            _repository.Add(value);
        }

        // DELETE api/books/5
        public void Delete(int id)
        {
            _repository.Delete(id);
        }
    }
}