﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using Database;
using Interfaces;
using Model;

namespace Lab6.Controllers
{
    public class AuthorsController : ApiController
    {
        private readonly IAuthorsRepository _repository;

        public AuthorsController()
        {
            _repository = new AuthorsRepository();
        }

        // GET api/authors
        public IEnumerable<Author> Get()
        {
            return _repository.GetAll();
        }

        // GET api/authors/5
        public Author Get(int id)
        {
            return _repository.Get(id);
        }

        public IEnumerable<Author> Get([FromUri] string search)
        {
            return _repository.GetAll().FindAll(x => x.AuthorSurname.Contains(search));
        }

        // POST api/authors
        public int Post([FromUri]Author value)
        {
            _repository.Add(value);
            return value.Id;
        }

        // PUT api/authors/5
        public void Put(int id, [FromUri]Author value)
        {
            value.Id = id;
            _repository.Add(value);
        }

        // DELETE api/authors/5
        public void Delete(int id)
        {
            _repository.Delete(id);
        }
    }
}
