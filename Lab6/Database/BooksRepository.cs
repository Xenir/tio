﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Interfaces;
using LiteDB;
using Model;

namespace Database
{
    public class BooksRepository : IBooksRepository
    {
        private readonly string _bookConnection = DatabaseConnections.BookConnection;

        public List<Book> GetAll()
        {
            using (var db = new LiteDatabase(_bookConnection))
            {
                var repository = db.GetCollection<Book>("books");
                var results = repository.FindAll();

                return results.ToList();
            }
        }

        public int Add(Book book)
        {
            using (var db = new LiteDatabase(_bookConnection))
            {
                var repository = db.GetCollection<Book>("books");
                repository.Insert(book);

                return book.Id;
            }
        }

        public Book Get(int id)
        {
            using (var db = new LiteDatabase(_bookConnection))
            {
                var repository = db.GetCollection<Book>("books");
                return repository.FindById(id);
            }
        }

        public Book Update(Book book)
        {
            using (var db = new LiteDatabase(_bookConnection))
            {
                var repository = db.GetCollection<Book>("books");
                if (repository.Update(book))
                    return book;
                else
                    return null;
            }
        }

        public bool Delete(int id)
        {
            using (var db = new LiteDatabase(_bookConnection))
            {
                var repository = db.GetCollection<Book>("books");
                return repository.Delete(id);
            }
        }
    }
}
