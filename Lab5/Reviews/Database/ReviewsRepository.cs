﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Interfaces;
using LiteDB;
using Model;
using Database.Model;

namespace Database
{
    public class ReviewsRepository : IReviewsRepository
    {
        private readonly string _reviewConnection = DatabaseConnections.ReviewConnection;

        public List<Review> GetAll()
        {
            using (var db = new LiteDatabase(this._reviewConnection))
            {
                var repository = db.GetCollection<ReviewDB>("reviews");
                var results = repository.FindAll();

                return results.Select(x => Map(x)).ToList();
            }
        }

        public int Add(Review review)
        {
            using (var db = new LiteDatabase(this._reviewConnection))
            {
                var dbObject = InverseMap(review);


                var repository = db.GetCollection<ReviewDB>("reviews");
                repository.Insert(dbObject);

                return dbObject.Id;
            }
        }

        public Review Get(int id)
        {
            using (var db = new LiteDatabase(this._reviewConnection))
            {
                var repository = db.GetCollection<ReviewDB>("reviews");
                var result = repository.FindById(id);
                return Map(result);
            }
        }

        public Review Update(Review review)
        {
            using (var db = new LiteDatabase(this._reviewConnection))
            {
                var dbObject = InverseMap(review);

                var repository = db.GetCollection<ReviewDB>("reviews");
                if (repository.Update(dbObject))
                    return Map(dbObject);
                else
                    return null;
            }
        }

        public bool Delete(int id)
        {
            using (var db = new LiteDatabase(this._reviewConnection))
            {
                var repository = db.GetCollection<ReviewDB>("reviews");
                return repository.Delete(id);
            }
        }

        private Review Map(ReviewDB dbReview)
        {
            if (dbReview == null)
                return null;
            var personsRepo = new PersonsRepository();
            var author = personsRepo.Get(dbReview.AuthorsId);
            return new Review() { Id = dbReview.Id, Content = dbReview.Content, Score = dbReview.Score, Author = author, MovieId = dbReview.MovieId };
        }

        private ReviewDB InverseMap(Review review)
        {
            if (review == null)
                return null;
            return new ReviewDB()
            {
                Id = review.Id, Content = review.Content, Score = review.Score, AuthorsId = review.Author.Id, MovieId = review.MovieId
            };
        }
    }
}
