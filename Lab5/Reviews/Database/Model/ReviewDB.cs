﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Database.Model;

namespace Database.Model
{
    public class ReviewDB
    {
        public int Id { get; set; }

        public string Content { get; set; }

        public int Score { get; set; }

        public int AuthorsId { get; set; }

        public int MovieId { get; set; }
    }
}
