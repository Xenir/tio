﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Database;
using Interfaces;
using Model;

namespace CRUDService
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public class ReviewsService : IReviewsService
    {
        private readonly IReviewsRepository _reviewsRepository;
        private readonly IPersonsRepository _personsRepository;

        public ReviewsService()
        {
            this._reviewsRepository = new ReviewsRepository();
            this._personsRepository = new PersonsRepository();
        }

        public int AddReview(Review review)
        {
            return this._reviewsRepository.Add(review);
        }

        public Review GetReview(int id)
        {
            return this._reviewsRepository.Get(id);
        }

        public List<Review> GetAllReviews()
        {
            return this._reviewsRepository.GetAll();
        }

        public Review UpdateReview(Review review)
        {
            return this._reviewsRepository.Update(review);
        }

        public bool DeleteReview(int id)
        {
            return this._reviewsRepository.Delete(id);
        }

        public int AddPerson(Person person)
        {
            return this._personsRepository.Add(person);
        }

        public Person GetPerson(int id)
        {
            return this._personsRepository.Get(id);
        }

        public List<Person> GetAllPersons()
        {
            return this._personsRepository.GetAll();
        }

        public Person UpdatePerson(Person person)
        {
            return this._personsRepository.Update(person);
        }

        public bool DeletePerson(int id)
        {
            return this._personsRepository.Delete(id);
        }
    }
}
