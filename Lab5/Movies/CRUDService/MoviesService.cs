﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Database;
using Interfaces;
using Model;

namespace CRUDService
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public class MoviesService : IMoviesService
    {
        private readonly IMoviesRepository _moviesRepository;

        public MoviesService()
        {
            this._moviesRepository = new MoviesRepository();
        }

        public int AddMovie(Movie movie)
        {
            return this._moviesRepository.Add(movie);
        }

        public Movie GetMovie(int id)
        {
            return this._moviesRepository.Get(id);
        }

        public List<Movie> GetAllMovies()
        {
            return this._moviesRepository.GetAll();
        }

        public Movie UpdateMovie(Movie movie)
        {
            return this._moviesRepository.Update(movie);
        }

        public bool DeleteMovie(int id)
        {
            return this._moviesRepository.Delete(id);
        }

    }
}
