﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CRUDClient.Movies {
    using System.Runtime.Serialization;
    using System;
    
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="Movie", Namespace="http://schemas.datacontract.org/2004/07/Model")]
    [System.SerializableAttribute()]
    public partial class Movie : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private int IdField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private int ReleaseYearField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string TitleField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int Id {
            get {
                return this.IdField;
            }
            set {
                if ((this.IdField.Equals(value) != true)) {
                    this.IdField = value;
                    this.RaisePropertyChanged("Id");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int ReleaseYear {
            get {
                return this.ReleaseYearField;
            }
            set {
                if ((this.ReleaseYearField.Equals(value) != true)) {
                    this.ReleaseYearField = value;
                    this.RaisePropertyChanged("ReleaseYear");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Title {
            get {
                return this.TitleField;
            }
            set {
                if ((object.ReferenceEquals(this.TitleField, value) != true)) {
                    this.TitleField = value;
                    this.RaisePropertyChanged("Title");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="Movies.IMoviesService")]
    public interface IMoviesService {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMoviesService/AddMovie", ReplyAction="http://tempuri.org/IMoviesService/AddMovieResponse")]
        int AddMovie(CRUDClient.Movies.Movie movie);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMoviesService/AddMovie", ReplyAction="http://tempuri.org/IMoviesService/AddMovieResponse")]
        System.Threading.Tasks.Task<int> AddMovieAsync(CRUDClient.Movies.Movie movie);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMoviesService/GetMovie", ReplyAction="http://tempuri.org/IMoviesService/GetMovieResponse")]
        CRUDClient.Movies.Movie GetMovie(int id);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMoviesService/GetMovie", ReplyAction="http://tempuri.org/IMoviesService/GetMovieResponse")]
        System.Threading.Tasks.Task<CRUDClient.Movies.Movie> GetMovieAsync(int id);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMoviesService/GetAllMovies", ReplyAction="http://tempuri.org/IMoviesService/GetAllMoviesResponse")]
        CRUDClient.Movies.Movie[] GetAllMovies();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMoviesService/GetAllMovies", ReplyAction="http://tempuri.org/IMoviesService/GetAllMoviesResponse")]
        System.Threading.Tasks.Task<CRUDClient.Movies.Movie[]> GetAllMoviesAsync();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMoviesService/UpdateMovie", ReplyAction="http://tempuri.org/IMoviesService/UpdateMovieResponse")]
        CRUDClient.Movies.Movie UpdateMovie(CRUDClient.Movies.Movie movie);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMoviesService/UpdateMovie", ReplyAction="http://tempuri.org/IMoviesService/UpdateMovieResponse")]
        System.Threading.Tasks.Task<CRUDClient.Movies.Movie> UpdateMovieAsync(CRUDClient.Movies.Movie movie);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMoviesService/DeleteMovie", ReplyAction="http://tempuri.org/IMoviesService/DeleteMovieResponse")]
        bool DeleteMovie(int id);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMoviesService/DeleteMovie", ReplyAction="http://tempuri.org/IMoviesService/DeleteMovieResponse")]
        System.Threading.Tasks.Task<bool> DeleteMovieAsync(int id);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IMoviesServiceChannel : CRUDClient.Movies.IMoviesService, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class MoviesServiceClient : System.ServiceModel.ClientBase<CRUDClient.Movies.IMoviesService>, CRUDClient.Movies.IMoviesService {
        
        public MoviesServiceClient() {
        }
        
        public MoviesServiceClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public MoviesServiceClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public MoviesServiceClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public MoviesServiceClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public int AddMovie(CRUDClient.Movies.Movie movie) {
            return base.Channel.AddMovie(movie);
        }
        
        public System.Threading.Tasks.Task<int> AddMovieAsync(CRUDClient.Movies.Movie movie) {
            return base.Channel.AddMovieAsync(movie);
        }
        
        public CRUDClient.Movies.Movie GetMovie(int id) {
            return base.Channel.GetMovie(id);
        }
        
        public System.Threading.Tasks.Task<CRUDClient.Movies.Movie> GetMovieAsync(int id) {
            return base.Channel.GetMovieAsync(id);
        }
        
        public CRUDClient.Movies.Movie[] GetAllMovies() {
            return base.Channel.GetAllMovies();
        }
        
        public System.Threading.Tasks.Task<CRUDClient.Movies.Movie[]> GetAllMoviesAsync() {
            return base.Channel.GetAllMoviesAsync();
        }
        
        public CRUDClient.Movies.Movie UpdateMovie(CRUDClient.Movies.Movie movie) {
            return base.Channel.UpdateMovie(movie);
        }
        
        public System.Threading.Tasks.Task<CRUDClient.Movies.Movie> UpdateMovieAsync(CRUDClient.Movies.Movie movie) {
            return base.Channel.UpdateMovieAsync(movie);
        }
        
        public bool DeleteMovie(int id) {
            return base.Channel.DeleteMovie(id);
        }
        
        public System.Threading.Tasks.Task<bool> DeleteMovieAsync(int id) {
            return base.Channel.DeleteMovieAsync(id);
        }
    }
}
