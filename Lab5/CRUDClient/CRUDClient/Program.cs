﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CRUDClient.Movies;
using CRUDClient.Reviews;

namespace CRUDClient
{
    class Program
    {
        private static readonly IMoviesService _moviesService = new MoviesServiceClient();
        private static readonly IReviewsService _reviewsService = new ReviewsServiceClient();

        static void Main(string[] args)
        {
            if (_moviesService.GetAllMovies().Length < 3)
            {
                _moviesService.AddMovie(new Movie() { Id = 0, ReleaseYear = 1994, Title = "Pulp Fiction" });
                _moviesService.AddMovie(new Movie() { Id = 0, ReleaseYear = 1999, Title = "Fight Club" });
                _moviesService.AddMovie(new Movie() { Id = 0, ReleaseYear = 2005, Title = "Sin City" });
            }
            var person = GetLoginData();
            var id = Login(person);
            person.Id = id;
            bool isRunning = true;
            while (isRunning)
            {
                PrintMainMenu();
                var answer = Console.ReadKey().KeyChar;
                Console.WriteLine();
                Console.WriteLine();
                switch (answer)
                {
                    case 'a':
                        AddReview(person);
                        break;
                    case 'b':
                        UpdateReview(person);
                        break;
                    case 'c':
                        DeleteReview(person);
                        break;
                    case 'd':
                        ShowMovieReviews();
                        break;
                    case 'e':
                        AddMovie();
                        break;
                    case 'f':
                        isRunning = false;
                        break;
                    default:
                        Console.WriteLine("Niewłaściwa opcja, powrót do menu głównego");
                        Console.WriteLine();
                        break;
                }
            }
        }

        private static Person GetLoginData()
        {
            Console.WriteLine("Podaj imię i nazwisko");
            var input = Console.ReadLine();
            var name = input.Split(' ');
            return new Person() { Id = 0, Name = name[0], Surname = name[1]};
        }

        private static int Login(Person person)
        {
            var persons = new List<Person>(_reviewsService.GetAllPersons());
            var filtered = persons.FindAll(x => x.Name == person.Name && x.Surname == person.Surname);
            if (filtered.Count > 0)
                return filtered[0].Id;
            return _reviewsService.AddPerson(person);
        }

        private static void PrintMainMenu()
        {
            Console.WriteLine("Wybierz akcje:");
            Console.WriteLine("a) dodanie recenzji");
            Console.WriteLine("b) edycja recenzji");
            Console.WriteLine("c) usuniecie edycji");
            Console.WriteLine("d) zobacz recenzje dla filmow");
            Console.WriteLine("e) dodaj film");
            Console.WriteLine("f) wyjscie");
            Console.WriteLine();
        }

        private static void AddReview(Person author)
        {
            var movies = new List<Movie>(_moviesService.GetAllMovies());
            foreach (var movie in movies)
            {
                Console.WriteLine("{0}) {1} - {2}", movie.Id, movie.Title, movie.ReleaseYear);
            }
            Console.WriteLine();
            Console.WriteLine("Wpisz numer filmu, ktory chcesz ocenic");
            var id = 0;
            try
            {
                id = int.Parse(Console.ReadLine());
                Console.WriteLine();
            }
            catch (FormatException ex)
            {
                Console.WriteLine("Blad danych wejsciowych, powrot do menu glownego");
                Console.WriteLine();
                return;
            }
            if (_moviesService.GetMovie(id) == null)
            {
                Console.WriteLine("Wybrano niewlasciwy numer, powrot do menu glownego");
                Console.WriteLine();
                return;
            }
            Console.WriteLine("Wpisz swoja recenzje, nacisniecie Enter zakonczy wpisywanie");
            var content = Console.ReadLine();
            Console.WriteLine();
            Console.WriteLine("Podaj swoja ocene w skali 0-100");
            var score = 0;
            try
            {
                score = int.Parse(Console.ReadLine());
                Console.WriteLine();
            }
            catch (FormatException ex)
            {
                Console.WriteLine("Blad danych wejsciowych, powrot do menu glownego");
                Console.WriteLine();
                return;
            }
            if (score < 0 || score > 100)
            {
                Console.WriteLine("Ocena poza skala, powrot do menu glownego");
                Console.WriteLine();
                return;
            }
            var review = new Review() { Author = author, Content = content, Id = 0, MovieId = id, Score = score };
            _reviewsService.AddReview(review);
            Console.WriteLine("Dodano nowa recenzje");
            Console.WriteLine();
        }

        private static void UpdateReview(Person author)
        {
            var reviews = GetReviewsByAuthorId(author.Id);
            if (reviews.Count == 0)
            {
                Console.WriteLine("Brak recenzji dla aktualnego autora, powrot do menu glownego");
                Console.WriteLine();
                return;
            }
            PrintReviews(reviews);
            Console.WriteLine();
            Console.WriteLine("Wybierz numer recenzji");
            var id = 0;
            try
            {
                id = int.Parse(Console.ReadLine());
                Console.WriteLine();
            }
            catch (FormatException ex)
            {
                Console.WriteLine();
                Console.WriteLine("Blad danych wejsciowych, powrot do menu glownego");
                Console.WriteLine();
                return;
            }
            var review = _reviewsService.GetReview(id);
            if (review == null)
            {
                Console.WriteLine("Nieprawidlowy numer recenzji, powrot do menu glownego");
                Console.WriteLine();
                return;
            }
            Console.WriteLine();
            Console.WriteLine("Poprzednia recenzja:");
            Console.WriteLine(review.Content);
            Console.WriteLine("Podaj nowa tresc");
            var newContent = Console.ReadLine();
            Console.WriteLine();
            Console.WriteLine("Podaj swoja ocene w skali 0-100");
            var newScore = 0;
            try
            {
                newScore = int.Parse(Console.ReadLine());
                Console.WriteLine();
            }
            catch (FormatException ex)
            {
                Console.WriteLine();
                Console.WriteLine("Blad danych wejsciowych, powrot do menu glownego");
                Console.WriteLine();
                return;
            }
            if (newScore < 0 || newScore > 100)
            {
                Console.WriteLine("Ocena poza skala, powrot do menu glownego");
                Console.WriteLine();
                return;
            }
            var newReview = new Review() {Author = author, Content = newContent, Id = id, MovieId = review.MovieId, Score = newScore };
            _reviewsService.UpdateReview(newReview);
            Console.WriteLine("Poprawnie zedytowano recenzje");
            Console.WriteLine();
        }

        private static void DeleteReview(Person author)
        {
            var reviews = GetReviewsByAuthorId(author.Id);
            if (reviews.Count == 0)
            {
                Console.WriteLine("Brak recenzji dla aktualnego autora, powrot do menu glownego");
                Console.WriteLine();
                return;
            }
            PrintReviews(reviews);
            Console.WriteLine();
            Console.WriteLine("Wybierz numer recenzji");
            var id = 0;
            try
            {
                id = int.Parse(Console.ReadLine());
                Console.WriteLine();
            }
            catch (FormatException ex)
            {
                Console.WriteLine();
                Console.WriteLine("Blad danych wejsciowych, powrot do menu glownego");
                Console.WriteLine();
                return;
            }
            var review = _reviewsService.GetReview(id);
            if (review == null)
            {
                Console.WriteLine("Nieprawidlowy numer recenzji, powrot do menu glownego");
                Console.WriteLine();
                return;
            }
            var movie = _moviesService.GetMovie(review.MovieId);
            Console.WriteLine("Na pewno chcesz usunac recenzje dla filmu {0}? Odpowiedz T/N (domyslnie N)", movie.Title);
            var key = Console.ReadKey().KeyChar;
            Console.WriteLine();
            if (key == 't')
            {
                _reviewsService.DeleteReview(id);
                Console.WriteLine("Poprawnie usunieto recenzje, powrot do menu glownego");
                Console.WriteLine();
                return;
            }
            Console.WriteLine("Anulowano usuwanie recenzji, powrot do menu glownego");
            Console.WriteLine();
        }

        private static void ShowMovieReviews()
        {
            var movies = new List<Movie>(_moviesService.GetAllMovies());
            var reviews = new List<Review>(_reviewsService.GetAllReviews());
            foreach (var movie in movies)
            {
                Console.WriteLine(movie.Title);
                var rev = reviews.FindAll(x => x.MovieId == movie.Id);
                if (rev.Count == 0)
                {
                    Console.WriteLine("Film nie posiada zadnych recenzji");
                    Console.WriteLine();
                    continue;
                }
                var avg = rev.Average(x => x.Score);
                Console.WriteLine("Srednia ocen: {0}", avg);
                foreach (var review in rev)
                {
                    Console.WriteLine("{0} {1} napisal(a):", review.Author.Name, review.Author.Surname);
                    Console.WriteLine(review.Content);
                    Console.WriteLine();
                }
            }
            Console.WriteLine();
        }

        private static void AddMovie()
        {
            Console.WriteLine("Podaj tytul");
            var title = Console.ReadLine();
            Console.WriteLine("Podaj rok produkcji");
            var year = 0;
            try
            {
                year = int.Parse(Console.ReadLine());
                Console.WriteLine();
            }
            catch (FormatException ex)
            {
                Console.WriteLine();
                Console.WriteLine("Blad danych wejsciowych, powrot do menu glownego");
                Console.WriteLine();
                return;
            }
            _moviesService.AddMovie(new Movie() {Id = 0, ReleaseYear = year, Title = title });
            Console.WriteLine("Poprawnie dodano nowy film");
            Console.WriteLine();
        }

        private static List<Review> GetReviewsByAuthorId(int authorId)
        {
            var reviews = new List<Review>(_reviewsService.GetAllReviews());
            return reviews.FindAll(x => x.Author.Id == authorId);
        }

        private static void PrintReviews(List<Review> reviews)
        {
            foreach (var review in reviews)
            {
                var movie = _moviesService.GetMovie(review.MovieId);
                Console.WriteLine("{0}) {1}", review.Id, movie.Title);
            }
        } 
    }
}
