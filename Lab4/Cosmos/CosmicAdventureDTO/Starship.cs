﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace CosmicAdventureDTO
{
    [DataContract]
    public class Starship
    {
        public Starship(List<Person> crew, int gold, int shipPower)
        {
            Crew = crew;
            Gold = gold;
            ShipPower = shipPower;
        }

        [DataMember]
        public List<Person> Crew { get; set; }

        [DataMember]
        public int Gold { get; set; }

        [DataMember]
        public int ShipPower { get; set; }
    }
}
