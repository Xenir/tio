﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace CosmicAdventureDTO
{
    [DataContract]
    public class Person
    {
        public Person(string name, string nick, float age)
        {
            Name = name;
            Nick = nick;
            Age = age;
        }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Nick { get; set; }

        [DataMember]
        public float Age { get; set; }
    }
}
