﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace CosmicAdventureDTO
{
    [DataContract]
    public class SpaceSystem
    {
        public SpaceSystem(string name, int minShipPower, int baseDistance, int gold)
        {
            Name = name;
            MinShipPower = minShipPower;
            BaseDistance = baseDistance;
            Gold = gold;
        }

        [DataMember]
        public string Name { get; set; }

        public int MinShipPower { get; }

        [DataMember]
        public int BaseDistance { get; set; }

        public int Gold { get; }
    }
}
