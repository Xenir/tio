﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using CosmicAdventureDTO;

namespace Cosmos
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public class Cosmos : ICosmos
    {
        private List<SpaceSystem> _systems;

        public void InitializeGame()
        {
            _systems = new List<SpaceSystem>();
            Random rnd = new Random();
            var names = new string[] { "Milky Way", "Disc World", "Galaxy Far Far Away", "Among the Stars" };
            for (var i = 0; i < 4; i++)
            {
                var minShipPower = rnd.Next(10, 40);
                var baseDistance = rnd.Next(20, 120);
                var gold = rnd.Next(3000, 7000);
                _systems.Add(new SpaceSystem(names[i], minShipPower, baseDistance, gold));
            }
        }

        public Starship SendStarship(Starship starship, string systemName)
        {
            var system = _systems.Find(x => x.Name == systemName);
            if (system == null)
            {
                starship.Crew.Clear();
            }
            else
            {
                var additionalAge = 0;
                if (starship.ShipPower <= 20)
                {
                    additionalAge = 2 * system.BaseDistance / 12;
                }
                else
                {
                    if (starship.ShipPower > 20 && starship.ShipPower <= 30)
                    {
                        additionalAge = 2 * system.BaseDistance / 6;
                    }
                    else
                    {
                        if (starship.ShipPower > 30)
                        {
                            additionalAge = 2 * system.BaseDistance / 4;
                        }

                    }
                }
                starship.Crew.ForEach(x => x.Age += additionalAge);
                starship.Crew.RemoveAll(x => x.Age > 90);
                if (starship.ShipPower >= system.MinShipPower)
                {
                    starship.Gold += system.Gold;
                    _systems.Remove(system);
                }
            }
            return starship;
        }

        public SpaceSystem GetSystem()
        {
            return _systems.FirstOrDefault();
        }

        public Starship GetStarship(int money)
        {
            var rnd = new Random();
            var firstName = new string[] { "Jonathan", "Gaius", "Adam", "Luke" };
            var secondName = new string[] { "Starseeker", "Dent", "Shepard", "Archer", "Skywalker", "Baltar" };
            var age = 20;
            var gold = 0;
            var shipPower = 0;
            if (money >= 1000 && money <= 3000)
            {
                shipPower = rnd.Next(10, 25);
            }
            else if (money > 3000 && money <= 10000)
            {
                shipPower = rnd.Next(20, 35);
            }
            else if (money > 10000)
            {
                shipPower = rnd.Next(35, 60);
            }
            var crew = new List<Person>();
            var crewSize = rnd.Next(1, 4);
            for (int i = 0; i < crewSize; i++)
            {
                var first = firstName[rnd.Next(firstName.Length - 1)];
                var second = secondName[rnd.Next(secondName.Length - 1)];
                crew.Add(new Person(first, second, age));
            }
            return new Starship(crew, gold, shipPower);
        }
    }
}
