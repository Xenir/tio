﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Client.Cosmos;
using Client.FirstOrder;

namespace Client
{
    class Program
    {
        private static List<Starship> _starships = new List<Starship>();

        private static bool _anySystem = true;

        private static int _gold = 1000;

        private static int _imperiumMoneyAskCount = 4;

        public static void Main(string[] args)
        {
            ICosmos cosmos = new CosmosClient();
            IFirstOrder order = new FirstOrderClient();
            cosmos.InitializeGame();
            bool isRunning = true;
            while (isRunning)
            {
                PrintMainMenu();
                var answer = Console.ReadKey().KeyChar;
                Console.WriteLine();
                Console.WriteLine();
                switch (answer)
                {
                    case 'a':
                        AskForGold(order);
                        break;
                    case 'b':
                        BuyShip(cosmos);
                        break;
                    case 'c':
                        SendShip(cosmos);
                        break;
                    case 'd':
                        FinishGame();
                        isRunning = false;
                        break;
                    default:
                        Console.WriteLine("Niewłaściwa opcja, powrót do menu głównego");
                        Console.WriteLine();
                        break;
                }
            }
            Console.ReadKey();
        }

        private static void PrintMainMenu()
        {
            Console.WriteLine("Złoto: {0}", _gold);
            Console.WriteLine("Ilość próśb o złoto do imperium: {0}", _imperiumMoneyAskCount);
            Console.WriteLine("a) Poproś imperium o złoto");
            Console.WriteLine("b) Kup statek za złoto");
            Console.WriteLine("c) Wyślij statek do systemu");
            Console.WriteLine("d) Zakończ grę");
            Console.WriteLine();
        }

        private static void AskForGold(IFirstOrder order)
        {
            if (_imperiumMoneyAskCount > 0)
            {
                var gold = order.GetMoneyFromImperium();
                _gold += gold;
                _imperiumMoneyAskCount--;
                Console.WriteLine("Pobrano {0} złota od imperium", gold);
                Console.WriteLine();
            }
            else
            {
                Console.WriteLine("Kredyt imperialny został wyczerpany, powrót do głownego menu");
                Console.WriteLine();
            }
        }

        private static void BuyShip(ICosmos cosmos)
        {
            Console.Write("Aktualne złoto: {0}. Za ile złota chcesz kupić statek? ", _gold);
            var gold = 0;
            try
            {
                gold = int.Parse(Console.ReadLine());
                Console.WriteLine();
            }
            catch (FormatException ex)
            {
                Console.WriteLine();
                Console.WriteLine("Niewłaściwy format, powrót do menu głównego");
                Console.WriteLine();
                return;
            }
            if (gold < 1000 || gold > _gold)
            {
                Console.WriteLine("Podano niewłaściwą liczbę złota, powrót do menu głównego");
                Console.WriteLine();
                return;
            }
            var starship = cosmos.GetStarship(gold);
            _starships.Add(starship);
            _gold -= gold;
            Console.WriteLine("Zakupiono statek za {0} złota", gold);
            Console.WriteLine();
        }

        private static void SendShip(ICosmos cosmos)
        {
            var system = cosmos.GetSystem();
            if (system == null)
            {
                _anySystem = false;
                Console.WriteLine("Brak systemów, powrót do menu głównego");
                Console.WriteLine();
                return;
            }
            if (_starships.Count == 0)
            {
                Console.WriteLine("Brak statków, powrót do menu głównego");
                Console.WriteLine();
                return;
            }
            Console.WriteLine("System {0}, odległość {1}", system.Name, system.BaseDistance);
            Console.WriteLine("Statków już gotowych do podróży: {0}", _starships.Count);
            Console.WriteLine("Wybierz statek wpisując jego numer (wyjście - wpisanie litery \"e\"");
            PresentStarships();
            var answer = Console.ReadLine();
            Console.WriteLine();
            if (answer.Trim() == "e")
            {
                Console.WriteLine("Powrót do głównego menu");
                Console.WriteLine();
                return;
            }
            var shipNumber = 0;
            try
            {
                shipNumber = int.Parse(answer);
            }
            catch (FormatException ex)
            {
                Console.WriteLine("Niewłaściwy format, powrót do menu głównego");
                Console.WriteLine();
                return;
            }
            if (shipNumber < 1 || shipNumber > _starships.Count)
            {
                Console.WriteLine("Niewłaściwy numer statu, powrót do głównego menu");
                Console.WriteLine();
                return;
            }
            var ship = _starships[shipNumber - 1];
            _starships.RemoveAt(shipNumber - 1);

            var returned = cosmos.SendStarship(ship, system.Name);
            if (returned.Gold != 0)
            {
                _gold += returned.Gold;
            }
            if (returned.Crew.Length > 0)
            {
                _starships.Add(returned);
            }
        }

        private static void PresentStarships()
        {
            var numberOfShips = _starships.Count;
            for (int i = 1; i <= numberOfShips; i++)
            {
                Console.Write("{0}. {1}", i, _starships[i - 1].ShipPower);
                foreach (var person in _starships[i - 1].Crew)
                {
                    Console.Write(", {0} {1} {2}", person.Name, person.Nick, person.Age);
                }
                Console.WriteLine();
            }
        }

        private static void FinishGame()
        {
            if (_anySystem == false)
            {
                Console.WriteLine("Zwycięstwo");
            }
            else
            {
                Console.WriteLine("Porażka");
            }
        }
    }
}
