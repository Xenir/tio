﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Client.FirstOrder {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="FirstOrder.IFirstOrder")]
    public interface IFirstOrder {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IFirstOrder/GetMoneyFromImperium", ReplyAction="http://tempuri.org/IFirstOrder/GetMoneyFromImperiumResponse")]
        int GetMoneyFromImperium();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IFirstOrder/GetMoneyFromImperium", ReplyAction="http://tempuri.org/IFirstOrder/GetMoneyFromImperiumResponse")]
        System.Threading.Tasks.Task<int> GetMoneyFromImperiumAsync();
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IFirstOrderChannel : Client.FirstOrder.IFirstOrder, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class FirstOrderClient : System.ServiceModel.ClientBase<Client.FirstOrder.IFirstOrder>, Client.FirstOrder.IFirstOrder {
        
        public FirstOrderClient() {
        }
        
        public FirstOrderClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public FirstOrderClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public FirstOrderClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public FirstOrderClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public int GetMoneyFromImperium() {
            return base.Channel.GetMoneyFromImperium();
        }
        
        public System.Threading.Tasks.Task<int> GetMoneyFromImperiumAsync() {
            return base.Channel.GetMoneyFromImperiumAsync();
        }
    }
}
