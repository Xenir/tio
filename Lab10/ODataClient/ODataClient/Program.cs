﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ODataClientabc.Library;

namespace ODataClient
{
    class Program
    {
        static void Main(string[] args)
        {
            string serviceUri = "http://localhost:49847/";
            var container = new ODataClientabc.Default.Container(new Uri(serviceUri));

            Console.WriteLine("Lista gier:");
            foreach (var game in container.Games)
            {
                Console.WriteLine("{0}", game.Title);
            }
            Console.WriteLine();

            Console.WriteLine("Dodawanie gry:");
            container.AddToGames(new Game() {Id = 1, Title = "Eldritch Horror", CreatorCompany = "Fantasy Flight Games / Galakta", Year = 2013, AgeRate = 14});

            var responses = container.SaveChanges();
            foreach (var response in responses)
            {
                Console.WriteLine("Odpowiedź: {0}", response.StatusCode);
            }
            Console.WriteLine();

            Console.WriteLine("Lista gier:");
            foreach (var game in container.Games)
            {
                Console.WriteLine("{0}", game.Title);
            }
            Console.WriteLine();

            Console.WriteLine("Lista koszulek:");
            foreach (var gs in container.CardShirts)
            {
                Console.WriteLine("{0}", gs.Name);
            }

            Console.WriteLine("Lista sklepów:");
            foreach (var store in container.Stores)
            {
                Console.WriteLine("{0}, {1}", store.Name, store.Address);
            }
            Console.WriteLine();

            Console.WriteLine("Usuwanie jednego sklepu:");
            container.Stores.Where(x => x.Name == "REBEL").ToList().ForEach(x => container.DeleteObject(x));
            responses = container.SaveChanges();
            foreach (var response in responses)
            {
                Console.WriteLine("Response: {0}", response.StatusCode);
            }

            Console.WriteLine("Lista sklepów:");
            foreach (var store in container.Stores)
            {
                Console.WriteLine("{0}, {1}", store.Name, store.Address);
            }
            Console.WriteLine();

            Console.ReadKey();
        }
    }
}
