﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Query;
using System.Web.Http.OData.Routing;
using Library;

namespace WebApi.Controllers
{
    public class CardShirtController : ODataController
    {
        GamesContext db = new GamesContext();

        [EnableQuery]
        public IQueryable<CardShirt> Get()
        {
            return db.CardShirts;
        }

        [EnableQuery]
        public SingleResult<CardShirt> Get([FromODataUri] int key)
        {
            IQueryable<CardShirt> result = db.CardShirts.Where(p => p.Id == key);
            return SingleResult.Create(result);
        }

        public async Task<IHttpActionResult> Post(CardShirt cardShirt)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            db.CardShirts.Add(cardShirt);
            await db.SaveChangesAsync();
            return Created(cardShirt);
        }

        public async Task<IHttpActionResult> Put([FromODataUri] int key, CardShirt update)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (key != update.Id)
            {
                return BadRequest();
            }
            db.Entry(update).State = EntityState.Modified;
            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CardShirtExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return Updated(update);
        }

        public async Task<IHttpActionResult> Delete([FromODataUri] int key)
        {
            var product = await db.CardShirts.FindAsync(key);
            if (product == null)
            {
                return NotFound();
            }
            db.CardShirts.Remove(product);
            await db.SaveChangesAsync();
            return StatusCode(HttpStatusCode.NoContent);
        }

        private bool CardShirtExists(int key)
        {
            return db.CardShirts.Any(p => p.Id == key);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
