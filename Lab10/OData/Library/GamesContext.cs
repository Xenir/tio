﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library
{
    public class GamesContext : DbContext
    {
        public GamesContext()
            : base("GamesContext")
        {
            this.Configuration.LazyLoadingEnabled = false;
            this.Configuration.ProxyCreationEnabled = false;
        }

        public DbSet<Game> Games { get; set; }

        public DbSet<Store> Stores { get; set; }

        public DbSet<CardShirt> CardShirts { get; set; }
    }
}
