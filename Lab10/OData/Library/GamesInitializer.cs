﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library
{
    public class GamesInitializer : DropCreateDatabaseIfModelChanges<GamesContext>
    {
        protected override void Seed(GamesContext context)
        {
            var paintings = new List<Game>
            {
                new Game() {Title = "Neuroshima HEX!", CreatorCompany = "Portal Games", Year = 2006, AgeRate = 13},
                new Game() {Title = "Cytadela", CreatorCompany = "Fantasy Flight Games / Galakta", Year = 2000, AgeRate = 10},
                new Game() {Title = "Doomtown: Reloaded", CreatorCompany = "Alderac Entertainment Group", Year = 2014, AgeRate = 14},
                new Game() {Title = "Pośród Gwiazd", CreatorCompany = "Artipia Games / Portal Games", Year = 2012, AgeRate = 10}
            };
            paintings.ForEach(x => context.Games.Add(x));
            context.SaveChanges();

            var artists = new List<Store>
            {
                new Store() {Name = "Dragonus", Address = "dragonus.pl"},
                new Store() {Name = "Trzy Trolle", Address = "3trolle.pl"},
                new Store() {Name = "REBEL", Address = "rebel.pl"}
            };
            artists.ForEach(x => context.Stores.Add(x));
            context.SaveChanges();

            var shirts = new List<CardShirt>
            {
                new CardShirt() {Name = "FFG Standard Size"},
                new CardShirt() {Name = "UltraPro Tarot"},
                new CardShirt() {Name = "Mayday Mini Euro"}
            };
            artists.ForEach(x => context.Stores.Add(x));
            context.SaveChanges();
        }
    }
}
