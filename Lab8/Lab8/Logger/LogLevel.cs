﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Lab8.Logger
{
    public enum LogLevel
    {
        FATAL,
        ERROR,
        WARN,
        INFO,
        DEBUG
    }
}