﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab8.Logger
{
    public interface ILogger
    {
        void Write(LogLevel level, string message);
    }
}
