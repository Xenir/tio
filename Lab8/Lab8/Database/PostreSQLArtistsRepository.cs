﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Lab8.DAL;
using Lab8.Models;

namespace Lab8.Database
{
    public class PostreSqlArtistsRepository : IArtistsRepository
    {
        private readonly MuseumContext _context = new MuseumContext();

        public IQueryable<Artist> GetAll()
        {
            return _context.Artists;
        }

        public int Add(Artist artist)
        {
            _context.Artists.Add(artist);
            _context.SaveChanges();
            return artist.Id;
        }

        public Artist Get(int id)
        {
            return _context.Artists.Find(id);
        }

        public Artist Update(Artist artist)
        {
            _context.Entry(artist).State = EntityState.Modified;
            _context.SaveChanges();
            return artist;
        }

        public bool Delete(int id)
        {
            var artist = _context.Artists.Find(id);
            if (artist == null)
            {
                return false;
            }
            _context.Artists.Remove(artist);
            _context.SaveChanges();
            return true;
        }
    }
}