﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Lab8.Models;
using LiteDB;

namespace Lab8.Database
{
    public class LiteDbArtistsRepository : IArtistsRepository
    {
        private readonly string _artistConnection = DatabaseConnections.ArtistConnection;

        public IQueryable<Artist> GetAll()
        {
            using (var db = new LiteDatabase(_artistConnection))
            {
                var repository = db.GetCollection<Artist>("artists");
                var results = repository.FindAll();

                return results.ToList().AsQueryable();
            }
        }

        public int Add(Artist artist)
        {
            using (var db = new LiteDatabase(_artistConnection))
            {
                var repository = db.GetCollection<Artist>("artists");
                repository.Insert(artist);

                return artist.Id;
            }
        }

        public Artist Get(int id)
        {
            using (var db = new LiteDatabase(_artistConnection))
            {
                var repository = db.GetCollection<Artist>("artists");
                return repository.FindById(id);
            }
        }

        public Artist Update(Artist artist)
        {
            using (var db = new LiteDatabase(_artistConnection))
            {
                var repository = db.GetCollection<Artist>("artists");
                if (repository.Update(artist))
                    return artist;
                else
                    return null;
            }
        }

        public bool Delete(int id)
        {
            using (var db = new LiteDatabase(_artistConnection))
            {
                var repository = db.GetCollection<Artist>("artists");
                return repository.Delete(id);
            }
        }
    }
}