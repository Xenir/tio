﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Lab8.Models;
using LiteDB;

namespace Lab8.Database
{
    public class LiteDbPaintingsRepository : IPaintingsRepository
    {
        private readonly string _paintingConnection = DatabaseConnections.PaintingConnection;

        public IQueryable<Painting> GetAll()
        {
            using (var db = new LiteDatabase(_paintingConnection))
            {
                var repository = db.GetCollection<Painting>("paintings");
                var results = repository.FindAll();

                return results.ToList().AsQueryable();
            }
        }

        public int Add(Painting painting)
        {
            using (var db = new LiteDatabase(_paintingConnection))
            {
                var repository = db.GetCollection<Painting>("paintings");
                repository.Insert(painting);

                return painting.Id;
            }
        }

        public Painting Get(int id)
        {
            using (var db = new LiteDatabase(_paintingConnection))
            {
                var repository = db.GetCollection<Painting>("paintings");
                return repository.FindById(id);
            }
        }

        public Painting Update(Painting painting)
        {
            using (var db = new LiteDatabase(_paintingConnection))
            {
                var repository = db.GetCollection<Painting>("paintings");
                if (repository.Update(painting))
                    return painting;
                else
                    return null;
            }
        }

        public bool Delete(int id)
        {
            using (var db = new LiteDatabase(_paintingConnection))
            {
                var repository = db.GetCollection<Painting>("paintings");
                return repository.Delete(id);
            }
        }
    }
}