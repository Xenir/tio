﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Lab8.DAL;
using Lab8.Models;

namespace Lab8.Database
{
    public class PostreSqlPaintingsRepository : IPaintingsRepository
    {
        private readonly MuseumContext _context = new MuseumContext();

        public IQueryable<Painting> GetAll()
        {
            return _context.Paintings;
        }

        public int Add(Painting painting)
        {
            _context.Paintings.Add(painting);
            _context.SaveChanges();
            return painting.Id;
        }

        public Painting Get(int id)
        {
            return _context.Paintings.Find(id);
        }

        public Painting Update(Painting painting)
        {
            _context.Entry(painting).State = EntityState.Modified;
            _context.SaveChanges();
            return painting;
        }

        public bool Delete(int id)
        {
            var painting = _context.Paintings.Find(id);
            if (painting == null)
            {
                return false;
            }
            _context.Paintings.Remove(painting);
            _context.SaveChanges();
            return true;
        }
    }
}