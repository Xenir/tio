﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab8.Models;

namespace Lab8.Database
{
    public interface IArtistsRepository
    {
        IQueryable<Artist> GetAll();

        int Add(Artist artist);

        Artist Get(int id);

        Artist Update(Artist artist);

        bool Delete(int id);
    }
}
