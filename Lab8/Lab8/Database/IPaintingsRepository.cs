﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab8.Models;

namespace Lab8.Database
{
    public interface IPaintingsRepository
    {
        IQueryable<Painting> GetAll();

        int Add(Painting painting);

        Painting Get(int id);

        Painting Update(Painting painting);

        bool Delete(int id);
    }
}
