﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Lab8.Models;

namespace Lab8.DAL
{
    public class StoreInitializer : DropCreateDatabaseIfModelChanges<MuseumContext>
    {
        protected override void Seed(MuseumContext context)
        {
            var paintings = new List<Painting>
            {
                new Painting() {Title = "Impresja, wschód słońca", Year = 1872},
                new Painting() {Title = "Mona Lisa", Year = 1517},
                new Painting() {Title = "Panorama Racławicka", Year = 1894}
            };
            paintings.ForEach(x => context.Paintings.Add(x));
            context.SaveChanges();

            var artists = new List<Artist>
            {
                new Artist() {ArtistName = "Claude", ArtistSurname = "Monet"},
                new Artist() {ArtistName = "Leonardo", ArtistSurname = "da Vinci"},
                new Artist() {ArtistName = "Wojciech", ArtistSurname = "Kossak"}
            };
            artists.ForEach(x => context.Artists.Add(x));
            context.SaveChanges();
        }
    }
}