﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Lab8.Models;

namespace Lab8.DAL
{
    public class MuseumContext : DbContext
    {
        public MuseumContext()
            : base("MuseumContext")
        {
            this.Configuration.LazyLoadingEnabled = false;
            this.Configuration.ProxyCreationEnabled = false;
        }

        public DbSet<Painting> Paintings { get; set; }

        public DbSet<Artist> Artists { get; set; }

    }
}