﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Lab8.Database;
using Lab8.Logger;
using Lab8.Models;

namespace Lab8.Controllers
{
    public class ArtistsController : ApiController
    {
        private readonly IArtistsRepository _repository;
        private readonly ILogger _logger;

        public ArtistsController(IArtistsRepository repository, ILogger logger)
        {
            _repository = repository;
            _logger = logger;
        }

        // GET: api/Artists
        public IQueryable<Artist> Get()
        {
            _logger.Write(LogLevel.INFO, "GET for Artists was called");
            return _repository.GetAll();
        }

        // GET: api/Artists/5
        [ResponseType(typeof(Artist))]
        public IHttpActionResult Get(int id)
        {
            _logger.Write(LogLevel.INFO, "GET for Artists was called");
            var artist = _repository.Get(id);
            if (artist == null)
            {
                return NotFound();
            }
            return Ok(artist);
        }

        // POST: api/Artists
        [ResponseType(typeof(Artist))]
        public IHttpActionResult Post([FromUri]Artist artist)
        {
            _logger.Write(LogLevel.INFO, "POST for Artists was called");
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _repository.Add(artist);

            return CreatedAtRoute("DefaultApi", new { id = artist.Id }, artist);
        }

        // PUT: api/Artists/5
        public IHttpActionResult Put(int id, [FromUri]Artist artist)
        {
            _logger.Write(LogLevel.INFO, "PUT for Artists was called");
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != artist.Id)
            {
                return BadRequest();
            }

            _repository.Add(artist);

            if (!ArtistExists(id))
                return NotFound();

            return StatusCode(HttpStatusCode.NoContent);
        }

        // DELETE: api/Artists/5
        public IHttpActionResult Delete(int id)
        {
            _logger.Write(LogLevel.INFO, "DELETE for Artists was called");
            if (_repository.Delete(id))
                return Ok();
            else
                return NotFound();
        }

        private bool ArtistExists(int id)
        {
            return _repository.Get(id) != null;
        }
    }
}
