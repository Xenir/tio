﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Lab8.Database;
using Lab8.Logger;
using Lab8.Models;

namespace Lab8.Controllers
{
    public class PaintingsController : ApiController
    {
        private readonly IPaintingsRepository _repository;
        private readonly ILogger _logger;

        public PaintingsController(IPaintingsRepository repository, ILogger logger)
        {
            _repository = repository;
            _logger = logger;
        }

        // GET: api/Paintings
        public IQueryable<Painting> Get()
        {
            _logger.Write(LogLevel.INFO, "GET for Paintings was called");
            return _repository.GetAll();
        }

        // GET: api/Paintings/5
        [ResponseType(typeof(Painting))]
        public IHttpActionResult Get(int id)
        {
            _logger.Write(LogLevel.INFO, "GET for Paintings was called");
            var painting = _repository.Get(id);
            if (painting == null)
            {
                return NotFound();
            }
            return Ok(painting);
        }

        // POST: api/Paintings
        [ResponseType(typeof(Painting))]
        public IHttpActionResult Post([FromUri]Painting painting)
        {
            _logger.Write(LogLevel.INFO, "POST for Paintings was called");
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _repository.Add(painting);

            return CreatedAtRoute("DefaultApi", new { id = painting.Id }, painting);
        }

        // PUT: api/Paintings/5
        public IHttpActionResult Put(int id, [FromUri]Painting painting)
        {
            _logger.Write(LogLevel.INFO, "PUT for Paintings was called");
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != painting.Id)
            {
                return BadRequest();
            }

            _repository.Add(painting);

            if (!PaintingExists(id))
                return NotFound();

            return StatusCode(HttpStatusCode.NoContent);
        }

        // DELETE: api/Paintings/5
        public IHttpActionResult Delete(int id)
        {
            _logger.Write(LogLevel.INFO, "DELETE for Paintings was called");
            if (_repository.Delete(id))
                return Ok();
            else
                return NotFound();
        }

        private bool PaintingExists(int id)
        {
            return _repository.Get(id) != null;
        }
    }
}
